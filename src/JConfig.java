import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;

public class JConfig {
	
	File file;
	ArrayList<String> lines = new ArrayList<String>();
	
	void SetOutputFile(File f) throws IOException {
		if(!f.exists()) {
			f.createNewFile();
			System.out.println("Config File Not Found. Created New Config file.");
		}
		
		else {
			System.out.println("Config File Found. Using Existing Config File.");
		}
		
		file = f;
	}
	
	File GetOutputFile() {
		if (file == null) {
			return null;
		}
		else {
			return file;
		}
	}
	
	void AddEntry(String Section, String Key, String Value) throws IOException {
		BufferedReader fr = new BufferedReader(new FileReader(file));
		String line;
		
		String aSect = "[" + Section + "]";
		String config = Key + "=" + Value;
		
		while((line = fr.readLine()) != null) {
			lines.add(line);
		}
		
		if(lines.size() != 0) {
			
			for(int x = 0; x < lines.size(); x++) {
				if(lines.get(x).contentEquals(aSect)) {
					lines.add(x+1, "\n" + config);
					break;
				}
			}
		}
	}
	
	void WriteConfig() throws IOException {
		FileWriter fw = new FileWriter(file, false);
		StringBuilder str = new StringBuilder();
		
		for(int x = 0; x < lines.size(); x++) {
			System.out.println(lines.get(x));
			str.append(lines.get(x));
		}
		System.out.println(str.toString());
		fw.write(str.toString());
		
		fw.close();
	}
	
	public static void main(String[] args) throws IOException {
		JConfig jc = new JConfig();
		jc.SetOutputFile(new File("test.ini"));
		System.out.println(jc.GetOutputFile());
		jc.AddEntry("Test", "t", "v");
		jc.WriteConfig();
		
	}

}